<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'KeyCDN',
    'description' => 'KeyCDN adapter for seamless integration into TYPO3 CMS',
    'category' => 'fe',
    'version' => '12.0.2-dev',
    'state' => 'stable',
    'author' => 'Daniel Haring',
    'author_company' => 'Getdesigned GmbH',
    'author_email' => 'dh@getdesigned.at',
    'constraints' => [
        'depends' => [
            'core' => '12.4.0-12.4.99'
        ],
        'suggests' => [
            'focuspoint' => '6.0.0-6.99.99'
        ]
    ]
];