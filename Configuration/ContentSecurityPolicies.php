<?php

declare(strict_types=1);

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use Getdesigned\KeyCdn\Resource\Driver\KeyCdnProxyDriver;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Security\ContentSecurityPolicy\Directive;
use TYPO3\CMS\Core\Security\ContentSecurityPolicy\Mutation;
use TYPO3\CMS\Core\Security\ContentSecurityPolicy\MutationCollection;
use TYPO3\CMS\Core\Security\ContentSecurityPolicy\MutationMode;
use TYPO3\CMS\Core\Security\ContentSecurityPolicy\Scope;
use TYPO3\CMS\Core\Security\ContentSecurityPolicy\SourceScheme;
use TYPO3\CMS\Core\Security\ContentSecurityPolicy\UriValue;
use TYPO3\CMS\Core\Service\FlexFormService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Type\Map;

return (function() {

    $entryIdentifier = 'default_backend';
    $cache = GeneralUtility::makeInstance(CacheManager::class)->getCache('keycdn_csp');

    if ($cache->has($entryIdentifier)) {
        $sources = $cache->get($entryIdentifier);
    } else {
        $sources = [];
        $flexFormService = GeneralUtility::makeInstance(FlexFormService::class);
        $query = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('sys_file_storage');
        $statement = $query
            ->select('configuration')
            ->from('sys_file_storage')
            ->where($query->expr()->eq('driver', $query->createNamedParameter(KeyCdnProxyDriver::SHORT_NAME)))
            ->andWhere($query->expr()->eq('is_online', $query->createNamedParameter(1, PDO::PARAM_INT)))
            ->andWhere($query->expr()->neq('configuration', $query->createNamedParameter('')))
            ->executeQuery()
        ;
        while ($storage = $statement->fetchAssociative()) {
            $config = $flexFormService->convertFlexFormContentToArray($storage['configuration']);
            if (empty($baseUri = $config['baseUri'] ?? '')) {
                continue;
            }
            $uriParts = parse_url($baseUri);
            if (
                !isset($uriParts['scheme'])
                || ('http' !== $uriParts['scheme'] && 'https' !== $uriParts['scheme'])
                || !isset($uriParts['host'])
            ) {
                continue;
            }
            $baseUri = $uriParts['scheme'] . '://' . $uriParts['host'];
            if (isset($uriParts['port'])) {
                $baseUri .= ':' . $uriParts['port'];
            }
            $sources[] = $baseUri;
        }
        $cache->set($entryIdentifier, $sources);
    }

    $mutations = [];
    foreach ($sources as $source) {
        $mutations[] = new Mutation(
            MutationMode::Extend,
            Directive::ImgSrc,
            SourceScheme::data,
            new UriValue($source)
        );
    }

    return Map::fromEntries([
        Scope::backend(),
        new MutationCollection(...$mutations)
    ]);
})();