<?php

declare(strict_types=1);

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

    // Resource Drivers
$GLOBALS['TYPO3_CONF_VARS']['SYS']['fal']['registeredDrivers']['KeyCdnProxy'] = [
    'class' => \Getdesigned\KeyCdn\Resource\Driver\KeyCdnProxyDriver::class,
    'shortName' => \Getdesigned\KeyCdn\Resource\Driver\KeyCdnProxyDriver::SHORT_NAME,
    'flexFormDS' => 'FILE:EXT:key_cdn/Configuration/Resource/Driver/KeyCdnDriverFlexForm.xml',
    'label' => 'Local file system with KeyCDN proxy'
];

    // File Renderers
$rendererRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Resource\Rendering\RendererRegistry::class);
$rendererRegistry->registerRendererClass(\Getdesigned\KeyCdn\Resource\Rendering\ImageTagRenderer::class);
unset($rendererRegistry);

    // File Processors
$GLOBALS['TYPO3_CONF_VARS']['SYS']['fal']['processors']['KeyCdnImageProcessor'] = [
    'className' => \Getdesigned\KeyCdn\Resource\Processing\KeyCdnImageProcessor::class,
    'after' => [
        'OnlineMediaPreviewProcessor'
    ],
    'before' => [
        'LocalImageProcessor'
    ]
];

    // Caches
$GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['keycdn_csp'] ??= [
    'frontend' => \TYPO3\CMS\Core\Cache\Frontend\VariableFrontend::class,
    'backend' => \TYPO3\CMS\Core\Cache\Backend\Typo3DatabaseBackend::class
];

    // Class Overrides
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Core\Resource\Service\ImageProcessingService::class] = [
    'className' => \Getdesigned\KeyCdn\Resource\Service\ImageProcessingService::class
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper::class] = [
    'className' => \Getdesigned\KeyCdn\ViewHelpers\ImageViewHelper::class
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Fluid\ViewHelpers\Uri\ImageViewHelper::class] = [
    'className' => \Getdesigned\KeyCdn\ViewHelpers\Uri\ImageViewHelper::class
];