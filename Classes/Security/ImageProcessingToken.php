<?php

declare(strict_types=1);
namespace Getdesigned\KeyCdn\Security;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use InvalidArgumentException;
use TYPO3\CMS\Core\Resource\ResourceInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * A Secure Token for signing KeyCDN image processing instructions.
 *
 * @since 12.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
class ImageProcessingToken
{
    /**
     * Creates an image processing token from a resource.
     *
     * @param ResourceInterface $resource The resource to create the image processing token from
     * @return self The image processing token created
     */
    public static function fromResource(ResourceInterface $resource): self
    {
        $storage = $resource->getStorage();

        if (empty($secureTokenKey = $storage->getConfiguration()['secureTokenKey'] ?? '')) {
            throw new InvalidArgumentException('No Secure Token Key specified for resource "' . $resource->getName() . '".', 1698767480);
        }

        $urlParts = parse_url($storage->getPublicUrl($resource));
        parse_str($urlParts['query'] ?? '', $processingInstructions);

        return GeneralUtility::makeInstance(
            self::class,
                $urlParts['path'] ?? '/',
            $secureTokenKey,
            $processingInstructions
        );
    }

    /**
     * Instantiates an image processing token.
     *
     * @param string $path The path of the image
     * @param string $secureTokenKey The secret to use for signing the image processing instructions
     * @param array $processingInstructions The processing instructions to apply
     */
    public function __construct(
        private readonly string $path,
        private readonly string $secureTokenKey,
        private array $processingInstructions = []
    ) {
        unset($this->processingInstructions['token']);
    }

    /**
     * Creates a Base64 MD5 hash for use as KeyCDN Secure Image Processing Token.
     *
     * @see: https://www.keycdn.com/support/secure-token
     * @return string The hash created
     */
    public function toBase64Hash(): string
    {
        return str_replace(['+', '/', '='], ['-', '_', ''], base64_encode(md5(sprintf(
            '%s%s%s',
            '/' . ltrim($this->path, '/'),
            str_replace(['%2C'], [','], http_build_query($this->processingInstructions, '', '&')),
            $this->secureTokenKey
        ), true)));
    }

    /**
     * Returns the path of the image.
     *
     * @return string The path of the image
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Returns the processing instructions to apply.
     *
     * @return array The processing instructons to apply
     */
    public function getProcessingInstructions(): array
    {
        return $this->processingInstructions;
    }

    /**
     * Derives a new token instance with a different path.
     *
     * @param string $path The new path to set
     * @return self The new token instance created
     */
    public function withPath(string $path): self
    {
        return GeneralUtility::makeInstance(self::class, $path, $this->secureTokenKey, $this->processingInstructions);
    }

    /**
     * Derives a new token instance with a different signing secret.
     *
     * @param string $secureTokenKey The new signing secret to set
     * @return self The new token instance created
     */
    public function withSecret(string $secureTokenKey): self
    {
        return GeneralUtility::makeInstance(self::class, $this->path, $secureTokenKey, $this->processingInstructions);
    }

    /**
     * Derives a new token instance with different processing instructions.
     *
     * @param array $processingInstructions The new processing instructons to set
     * @return self The new token instance created
     */
    public function withProcessingInstructions(array $processingInstructions): self
    {
        return GeneralUtility::makeInstance(self::class, $this->path, $this->secureTokenKey, $processingInstructions);
    }

    /**
     * Returns the string representation of the token.
     *
     * @return string The string representation of the token
     */
    public function __toString(): string
    {
        return $this->toBase64Hash();
    }
}