<?php

declare(strict_types=1);
namespace Getdesigned\KeyCdn\Resource\Service;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use Getdesigned\KeyCdn\Resource\Event\BeforeImageProcessingEvent;
use Psr\EventDispatcher\EventDispatcherInterface;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\FileProcessingAspect;
use TYPO3\CMS\Core\Locking\ResourceMutex;
use TYPO3\CMS\Core\Resource\Exception\FileAlreadyProcessedException;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use TYPO3\CMS\Core\Resource\ProcessedFileRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Processes a preprocessed processed file.
 *
 * @since 12.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
class ImageProcessingService extends \TYPO3\CMS\Core\Resource\Service\ImageProcessingService
{
    /**
     * Event dispatcher
     *
     * @var EventDispatcherInterface
     */
    private EventDispatcherInterface $eventDispatcher;

    /**
     * Instantiates the image service.
     *
     * @param ProcessedFileRepository $processedFileRepository Repository for fetching processed files
     * @param Context $context TYPO3 CMS Context
     * @param ResourceMutex $locker Resource lock service
     */
    public function __construct(
        private readonly ProcessedFileRepository $processedFileRepository,
        private readonly Context $context,
        private readonly ResourceMutex $locker
    ) {
        parent::__construct($processedFileRepository, $context, $locker);

        $this->eventDispatcher = GeneralUtility::makeInstance(EventDispatcherInterface::class);
    }

    /**
     * Processes a file.
     *
     * @param int $processedFileId The identifier of the file to process
     * @return ProcessedFile The processed file
     */
    public function process(int $processedFileId): ProcessedFile
    {
        $processedFile = $this->processedFileRepository->findByUid($processedFileId);

        $event = $this->eventDispatcher->dispatch(new BeforeImageProcessingEvent($processedFile));
        $processedFile = $event->getProcessedFile();

        try {
            $this->validateProcessedFile($processedFile);
            $hadToWaitForLock = $this->locker->acquireLock(get_parent_class(), (string)$processedFileId);

            if ($hadToWaitForLock) {
                    // Fetch the processed file again, as it might have been processed by another process while waiting
                    // for the lock
                $processedFile = $this->processedFileRepository->findByUid($processedFileId);
                $this->validateProcessedFile($processedFile);
            }

            $this->context->setAspect('fileProcessing', new FileProcessingAspect(false));
            $processedFile = $processedFile->getOriginalFile()->process(
                $processedFile->getTaskIdentifier(),
                $processedFile->getProcessingConfiguration()
            );

            $this->validateProcessedFile($processedFile);

        } catch (FileAlreadyProcessedException $exception) {
            $processedFile = $exception->getProcessedFile();
        } finally {
            $this->locker->releaseLock(get_parent_class());
        }

        return $processedFile;
    }

    /**
     * Checks whether a processed file already was processed.
     *
     * @param ProcessedFile $processedFile The processed file to check
     * @return void
     * @throws FileAlreadyProcessedException
     */
    private function validateProcessedFile(ProcessedFile $processedFile): void
    {
        if ($processedFile->isProcessed()) {
            throw new FileAlreadyProcessedException($processedFile, 1698673004);
        }
    }
}