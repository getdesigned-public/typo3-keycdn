<?php

declare(strict_types=1);
namespace Getdesigned\KeyCdn\Resource\Rendering;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use Getdesigned\KeyCdn\Imaging\ImageManipulation\Point;
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use TYPO3\CMS\Core\Resource\Rendering\FileRendererInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Service\ImageService;
use TYPO3Fluid\Fluid\Core\ViewHelper\TagBuilder;

/**
 * Renders an <img />-tag.
 *
 * @since 12.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
class ImageTagRenderer implements FileRendererInterface
{
    /**
     * Attributes to directly pass to the tag.
     *
     * @var string[]
     */
    protected static array $tagAttributes = [
        'accesskey', 'class', 'dir', 'id', 'ismap', 'lang', 'longdesc', 'onclick', 'style', 'tabindex', 'title', 'usemap'
    ];

    /**
     * The service for handling images.
     *
     * @internal Use $this->getImageService() instead
     * @var ImageService|null
     */
    private ?ImageService $_imageService = null;

    /**
     * @inheritdoc
     */
    public function getPriority(): int
    {
        return 1;
    }

    /**
     * @inheritdoc
     */
    public function canRender(FileInterface $file): bool
    {
        return GeneralUtility::inList($GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'], $file->getExtension());
    }

    /**
     * @inheritdoc
     */
    public function render(FileInterface $file, $width, $height, array $options = []): string
    {
        $tag = $this->initializeTag('img', $options);
        $processedImage = $this->process($file, $options);
        $imageUri = $this->getImageService()->getImageUri($processedImage, (bool)($options['absolute'] ?? false));

        $tag->addAttribute('src', $imageUri);
        $tag->addAttribute('width', $processedImage->getProperty('width'));
        $tag->addAttribute('height', $processedImage->getProperty('height'));

        if (in_array($options['loading'] ?? '', ['lazy', 'eager', 'auto'], true)) {
            $tag->addAttribute('loading', $options['loading']);
        }

        if (in_array($options['decoding'] ?? '', ['sync', 'async', 'auto'], true)) {
            $tag->addAttribute('decoding', $options['decoding']);
        }

        $tag->addAttribute(
            'alt',
            ($options['alt'] ?? '') ?: ($file->hasProperty('alternative') ? $file->getProperty('alternative') : '')
        );

        if (!empty($title = ($options['title'] ?? '') ?: ($file->hasProperty('title') ? $file->getProperty('title') : ''))) {
            $tag->addAttribute('title', $title);
        }

        return $tag->render();
    }

    /**
     * Processes the given file.
     *
     * @param FileInterface $file The file to process
     * @param array $options The options to apply
     * @return ProcessedFile The processed file
     */
    public function process(FileInterface $file, array $options = []): ProcessedFile
    {
        $cropVariant = (string)(($options['cropVariant'] ?? '') ?: 'default');
        $cropString = $options['crop'] ?? null;
        if (null === $cropString && $file->hasProperty('crop')) {
            $cropString = $file->getProperty('crop');
        }
        if (is_array($cropString)) {
            $cropString = json_encode($cropString);
        }
        $cropVariantCollection = CropVariantCollection::create((string)$cropString);
        $cropArea = $cropVariantCollection->getCropArea($cropVariant);

        $processingInstructions = [
            'crop' => $cropArea->isEmpty() ? null : $cropArea->makeAbsoluteBasedOnFile($file)
        ];
        if (!empty($options['width'] ?? '')) {
            $processingInstructions['width'] = $options['width'];
        }
        if (!empty($options['height'] ?? '')) {
            $processingInstructions['height'] = $options['height'];
        }
        if (!empty($options['minWidth'] ?? '')) {
            $processingInstructions['minWidth'] = $options['minWidth'];
        }
        if (!empty($options['minHeight'] ?? '')) {
            $processingInstructions['minHeight'] = $options['minHeight'];
        }
        if (!empty($options['maxWidth'] ?? '')) {
            $processingInstructions['maxWidth'] = $options['maxWidth'];
        }
        if (!empty($options['maxHeight'] ?? '')) {
            $processingInstructions['maxHeight'] = $options['maxHeight'];
        }
        if (!empty($options['fileExtension'] ?? '')) {
            $processingInstructions['fileExtension'] = $options['fileExtension'];
        }
        if (!empty($focalPoint = Point::createFromFile($file))) {
            $processingInstructions['focus_point_x'] = $focalPoint->getOffsetLeft();
            $processingInstructions['focus_point_y'] = $focalPoint->getOffsetTop();
        }

        return $this->getImageService()->applyProcessingInstructions($file, $processingInstructions);
    }

    /**
     * Returns the service for handling images.
     *
     * @return ImageService The service for handling images
     */
    public function getImageService(): ImageService
    {
        $this->_imageService = $this->_imageService ?? GeneralUtility::makeInstance(ImageService::class);
        return $this->_imageService;
    }

    /**
     * Initializes the tag to render.
     *
     * @param string $tagName The name of the tag to render
     * @param array $arguments The arguments to apply
     * @return TagBuilder The tag to render
     */
    protected function initializeTag(string $tagName = 'div', array $arguments = []): TagBuilder
    {
        $tag = new TagBuilder('img');

        if (!empty($additionalAttributes = $arguments['additionalAttributes'] ?? null) && is_array($additionalAttributes)) {
            $tag->addAttributes($additionalAttributes);
        }

        if (!empty($data = $arguments['data'] ?? null) && is_array($data)) {
            foreach ($data as $dataAttributeName => $dataAttributeValue) {
                $tag->addAttribute('data-' . $dataAttributeName, $dataAttributeValue);
            }
        }

        if (!empty($aria = $arguments['aria'] ?? null) && is_array($aria)) {
            foreach ($aria as $ariaAttributeName => $ariaAttributeValue) {
                $tag->addAttribute('aria-' . $ariaAttributeName, $ariaAttributeValue);
            }
        }

        foreach (static::$tagAttributes as $attributeName) {
            if (!empty($attributeValue = ($arguments[$attributeName] ?? null))) {
                $tag->addAttribute($attributeName, $attributeValue);
            }
        }

        return $tag;
    }
}