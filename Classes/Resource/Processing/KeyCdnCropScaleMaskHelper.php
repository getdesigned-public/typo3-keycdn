<?php

declare(strict_types=1);
namespace Getdesigned\KeyCdn\Resource\Processing;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use Getdesigned\KeyCdn\Imaging\ImageManipulation\Point;
use InvalidArgumentException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Imaging\ImageDimension;
use TYPO3\CMS\Core\Resource\Processing\TaskInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;

/**
 * Helper class to perform a crop/scale/mask task through KeyCDN.
 *
 * @since 12.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
class KeyCdnCropScaleMaskHelper
{
    /**
     * Service for reading extension configurations.
     *
     * @var ExtensionConfiguration
     */
    protected ExtensionConfiguration $extensionConfiguration;

    /**
     * Instantiates the helper.
     */
    public function __construct()
    {
        $this->extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class);
    }

    /**
     * Calculates processing instructions for KeyCDN for a local file.
     *
     * @param TaskInterface $task The task to calculate the processing instructions from
     * @return array|null The processing instructions calculated, if any
     */
    public function process(TaskInterface $task): ?array
    {
        return $this->processWithLocalFile($task, $task->getSourceFile()->getForLocalProcessing(false));
    }

    /**
     * Actually calculates the processing instructions.
     *
     * @param TaskInterface $task The corresponding task
     * @param string $originalFileName Absolute path to the local image to be processed
     * @return array|null The processing instructions for KeyCDN
     */
    public function processWithLocalFile(TaskInterface $task, string $originalFileName): ?array
    {
        $targetFile = $task->getTargetFile();
        $configuration = $targetFile->getProcessingConfiguration();

        $processingInstructions = $this->processCropScale($task);

            // mask
        if (is_array($configuration['maskImages'] ?? null)) {
            throw new InvalidArgumentException('Masking images via KeyCDN is currently not supported.', 1695722760);
        }

            // specify quality and metadata
        if (!empty($processingInstructions)) {
            $processingInstructions['quality'] = MathUtility::forceIntegerInRange(
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['jpg_quality'],
                10,
                100,
                82
            );
            try {
                if ($this->extensionConfiguration->get('key_cdn', 'keepMetaData')) {
                    $processingInstructions['metadata'] = 1;
                }
            } catch (ExtensionConfigurationExtensionNotConfiguredException|ExtensionConfigurationPathDoesNotExistException $e) {}
        }

        return $processingInstructions;
    }

    /**
     * Calculates the processing instructions for cropping and scaling.
     *
     * @param TaskInterface $task The corresponding task
     * @return array The processing instructions calculated
     */
    protected function processCropScale(TaskInterface $task): array
    {
        $targetDimension = ImageDimension::fromProcessingTask($task);

            // focal point
        if (!empty($focalPoint = Point::createFromProcessingTask($task))) {
            return [
                'crop' => $focalPoint->toCropFocalPoint(),
                'width' => $targetDimension->getWidth(),
                'height' => $targetDimension->getHeight()
            ];
        }

        $processingInstructions = [];
        $sourceFile = $task->getSourceFile();
        $targetFile = $task->getTargetFile();
        $sourceDimension = new ImageDimension($sourceFile->getProperty('width'), $sourceFile->getProperty('height'));
        $configuration = $targetFile->getProcessingConfiguration();

            // scale
        if (!empty($configuration['crop']) || !$this->isSameAspectRatio($sourceDimension, $targetDimension)) {
            $processingInstructions['width'] = $targetDimension->getWidth();
            $processingInstructions['height'] = $targetDimension->getHeight();
            $processingInstructions['fit'] = 'cover';
        } elseif ($sourceDimension->getWidth() !== $targetDimension->getWidth()) {
            $processingInstructions['width'] = $targetDimension->getWidth();
        }

            // crop
        if (!empty($configuration['crop'])) {
            if (!empty($cropData = json_decode((string)$configuration['crop']))) {
                $offsetLeft = (int)($cropData->x ?? 0);
                $offsetTop = (int)($cropData->y ?? 0);
                $newWidth = (int)($cropData->width ?? 0);
                $newHeight = (int)($cropData->height ?? 0);
            } else {
                [$offsetLeft, $offsetTop, $newWidth, $newHeight] = explode(',', $configuration['crop'], 4);
            }
            $processingInstructions['crop'] = sprintf(
                '%s,%s,%s,%s',
                $newWidth,
                $newHeight,
                $offsetLeft,
                $offsetTop
            );
        }

        return $processingInstructions;
    }

    /**
     * Checks whether the aspect ratios of the given image dimensions are identical.
     *
     * @param ImageDimension $first The first dimensions to compare
     * @param ImageDimension $second The second dimensions to compare
     * @return bool TRUE if both aspect ratios are identical, otherwise FALSE
     */
    protected function isSameAspectRatio(ImageDimension $first, ImageDimension $second): bool
    {
        return ($first->getWidth() / $first->getHeight()) === ($second->getWidth() / $second->getHeight());
    }
}