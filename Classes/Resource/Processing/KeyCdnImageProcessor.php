<?php

declare(strict_types=1);
namespace Getdesigned\KeyCdn\Resource\Processing;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use Getdesigned\KeyCdn\Security\ImageProcessingToken;
use InvalidArgumentException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Http\Uri;
use TYPO3\CMS\Core\Imaging\ImageDimension;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\Processing\ProcessorInterface;
use TYPO3\CMS\Core\Resource\Processing\TaskInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use UnhandledMatchError;

/**
 * Process images through KeyCDN.
 *
 * @since 12.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
class KeyCdnImageProcessor implements ProcessorInterface
{
    /**
     * Service for reading extension configurations.
     *
     * @var ExtensionConfiguration
     */
    protected ExtensionConfiguration $extensionConfiguration;

    /**
     * Instantiates the processor.
     */
    public function __construct()
    {
        $this->extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class);
    }

    /**
     * @inheritdoc
     */
    public function canProcessTask(TaskInterface $task): bool
    {
        $sourceFile = $task->getSourceFile();

        try {
            return
                !!$this->extensionConfiguration->get('key_cdn', 'enableCdnFiles')
                && 'Image' === $task->getType()
                && in_array($task->getName(), ['CropScaleMask', 'Preview'], true)
                && !!($sourceFile->getStorage()->getConfiguration()['nativeFileProcessing'] ?? false)
                && in_array($sourceFile->getStorage()->getDriverType(), ['KeyCdnProxy'], true)
                && File::FILETYPE_IMAGE === $sourceFile->getType()
                && $sourceFile->isImage()
            ;
        } catch (ExtensionConfigurationExtensionNotConfiguredException|ExtensionConfigurationPathDoesNotExistException $e) {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function processTask(TaskInterface $task): void
    {
        $sourceFile = $task->getSourceFile();
        $dimension = ImageDimension::fromProcessingTask($task);

        $processingUrl = new Uri($sourceFile->getPublicUrl());
        parse_str($processingUrl->getQuery(), $queryParams);
        unset($queryParams['token']);

        $processingInstructions = $this->getHelperByTaskName($task->getName())->process($task);
        if (!empty($secureTokenKey = $sourceFile->getStorage()->getConfiguration()['secureTokenKey'] ?? null)) {
            $processingInstructions['token'] = (string)GeneralUtility::makeInstance(
                ImageProcessingToken::class,
                $processingUrl->getPath(),
                $secureTokenKey,
                $processingInstructions
            );
        }

        $queryParams = array_merge_recursive($queryParams, $processingInstructions);
        $processingUrl = $processingUrl->withQuery(str_replace(['%2C'], [','], http_build_query($queryParams, '', '&')));

        $task->setExecuted(true);
        $task->getTargetFile()->updateProperties([
            'width' => $dimension->getWidth(),
            'height' => $dimension->getHeight(),
            'checksum' => $task->getConfigurationChecksum(),
            'processing_url' => (string)$processingUrl
        ]);

        $task->getTargetFile()->updateProcessingUrl((string)$processingUrl);
    }

    /**
     * Returns the helper for processing the task.
     *
     * @param string $taskName The name of the task to process
     * @return KeyCdnCropScaleMaskHelper|KeyCdnPreviewHelper The helper for processing the task
     */
    protected function getHelperByTaskName(string $taskName): KeyCdnCropScaleMaskHelper|KeyCdnPreviewHelper
    {
        try {
            return match ($taskName) {
                'CropScaleMask' => GeneralUtility::makeInstance(KeyCdnCropScaleMaskHelper::class),
                'Preview' => GeneralUtility::makeInstance(KeyCdnPreviewHelper::class)
            };
        } catch (UnhandledMatchError $e) {
            throw new InvalidArgumentException('Unable to find helper for task name "' . $taskName . '".', 1695728607);
        }
    }
}