<?php

declare(strict_types=1);
namespace Getdesigned\KeyCdn\Resource\Processing;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use Getdesigned\KeyCdn\Imaging\ImageManipulation\Point;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\Processing\LocalPreviewHelper;
use TYPO3\CMS\Core\Resource\Processing\TaskInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;

/**
 * Helper class to process preview images through KeyCDN.
 *
 * @since 12.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
class KeyCdnPreviewHelper
{
    /**
     * Service for reading extension configurations.
     *
     * @var ExtensionConfiguration
     */
    protected ExtensionConfiguration $extensionConfiguration;

    /**
     * Instantiates the helper.
     */
    public function __construct()
    {
        $this->extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class);
    }

    /**
     * Calculates processing instructions for KeyCDN for a local file.
     *
     * @param TaskInterface $task The task to calculate the processing instructions from
     * @return array|null The processing instructions calculated, if any
     */
    public function process(TaskInterface $task): ?array
    {
        $sourceFile = $task->getSourceFile();
        $configuration = LocalPreviewHelper::preProcessConfiguration($task->getConfiguration());

            // avoid up-scaling of previews
        if (
            $sourceFile->getProperty('width') > 0
            && $sourceFile->getProperty('height') > 0
            && $configuration['width'] > $sourceFile->getProperty('width')
            && $configuration['height'] > $sourceFile->getProperty('height')
        ) {
            return null;
        }

        $task->getTargetFile()->updateProcessingUrl('');
        return $this->generatePreviewFromLocalFile($sourceFile, $configuration);
    }

    /**
     * Calculates the processing instructions.
     *
     * @param File $file The file to process
     * @param array $configuration The configuration to apply
     * @return array|null The processing instructions calculated
     */
    protected function generatePreviewFromLocalFile(File $file, array $configuration): ?array
    {
        $processingInstructions = [
            'width' => $configuration['width'],
            'height' => $configuration['height'],
            'quality' => MathUtility::forceIntegerInRange(
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['jpg_quality'],
                10,
                100,
                82
            )
        ];

        if (!empty($focalPoint = Point::createFromFile($file))) {
            $processingInstructions['crop'] = $focalPoint->toCropFocalPoint();
        } else {
            $processingInstructions['fit'] = 'cover';
        }

        try {
            if ($this->extensionConfiguration->get('key_cdn', 'keepMetaData')) {
                $processingInstructions['metadata'] = 1;
            }
        } catch (ExtensionConfigurationExtensionNotConfiguredException|ExtensionConfigurationPathDoesNotExistException $e) {}

        return $processingInstructions;
    }
}