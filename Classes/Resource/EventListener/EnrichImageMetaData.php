<?php

declare(strict_types=1);
namespace Getdesigned\KeyCdn\Resource\EventListener;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use Getdesigned\KeyCdn\Resource\Driver\KeyCdnProxyDriver;
use TYPO3\CMS\Core\Resource\Event\AfterFileMetaDataCreatedEvent;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\Index\MetaDataRepository;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Type\File\ImageInfo;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Adds specific metadata to images handled through KeyCDN.
 *
 * @since 12.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
class EnrichImageMetaData
{
    /**
     * Handles the event.
     *
     * @param AfterFileMetaDataCreatedEvent $event The event passed
     * @return void
     */
    public function __invoke(AfterFileMetaDataCreatedEvent $event): void
    {
        if (
            empty($file = GeneralUtility::makeInstance(ResourceFactory::class)->getFileObject($event->getFileUid()))
            || File::FILETYPE_IMAGE !== $file->getType()
            || KeyCdnProxyDriver::SHORT_NAME !== $file->getStorage()->getDriverType()
        ) {
            return;
        }

        $metaDataRepository = GeneralUtility::makeInstance(MetaDataRepository::class);

        $fileNameAndPath = $file->getForLocalProcessing(false);
        $imageInfo = GeneralUtility::makeInstance(ImageInfo::class, $fileNameAndPath);

        $additionalMetaInformation = [
            'width' => $imageInfo->getWidth(),
            'height' => $imageInfo->getHeight()
        ];

        $metaDataRepository->update($file->getUid(), $additionalMetaInformation, $event->getRecord());
        $event->setRecord($metaDataRepository->findByFileUid($file->getUid()));
    }
}