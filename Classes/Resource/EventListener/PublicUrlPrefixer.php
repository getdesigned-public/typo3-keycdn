<?php

declare(strict_types=1);
namespace Getdesigned\KeyCdn\Resource\EventListener;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Http\ApplicationType;
use TYPO3\CMS\Core\Resource\Event\GeneratePublicUrlForResourceEvent;
use TYPO3\CMS\Core\Resource\ResourceInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * Prefixes file URLs to ensure absolute links.
 *
 * @since 12.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
class PublicUrlPrefixer
{
    /**
     * Static property to avoid an infinite loop.
     *
     * @var bool
     */
    private static bool $isProcessingUrl = false;

    /**
     * Main entry method. Handles the event.
     *
     * @param GeneratePublicUrlForResourceEvent $event The event object passed
     * @return void
     */
    public function __invoke(GeneratePublicUrlForResourceEvent $event): void
    {
        if (self::$isProcessingUrl || empty($applicationType = $this->getApplicationType())) {
            return;
        }

        $resource = $event->getResource();
        if (!$this->isKeyCdnResource($resource)) {
            return;
        }

        if ($applicationType->isFrontend() && !empty($frontendController = $this->getFrontendController())) {
            self::$isProcessingUrl = true;
            try {
                $originalUrl = $event->getStorage()->getPublicUrl($resource);
                if (!$originalUrl || PathUtility::hasProtocolAndScheme($originalUrl)) {
                    return;
                }
                $event->setPublicUrl($frontendController->absRefPrefix . $originalUrl);
                return;
            } finally {
                self::$isProcessingUrl = false;
            }
        }

        if ($applicationType->isBackend()) {
            self::$isProcessingUrl = true;
            try {
                $originalUrl = $event->getStorage()->getPublicUrl($resource);
                if (!$originalUrl || PathUtility::hasProtocolAndScheme($originalUrl)) {
                    return;
                }
                $event->setPublicUrl(GeneralUtility::getIndpEnv('TYPO3_SITE_PATH') . $originalUrl);
                return;
            } finally {
                self::$isProcessingUrl = false;
            }
        }
    }

    /**
     * Checks whether the given resource is handled through KeyCDN.
     *
     * @param ResourceInterface $resource The resource to check
     * @return bool TRUE if the resource is handled through KeyCDN, otherwise FALSE
     */
    private function isKeyCdnResource(ResourceInterface $resource): bool
    {
        return 'KeyCdnProxy' === $resource->getStorage()->getDriverType();
    }

    /**
     * Returns the current frontend controller.
     *
     * @return TypoScriptFrontendController|null The current frontend controller, if any
     */
    private function getFrontendController(): ?TypoScriptFrontendController
    {
        return $GLOBALS['TSFE'] ?? null;
    }

    /**
     * Returns the current application type.
     *
     * @return ApplicationType|null The current application type, if any
     */
    private function getApplicationType(): ?ApplicationType
    {
        if (!($GLOBALS['TYPO3_REQUEST'] ?? null) instanceof ServerRequestInterface) {
            return null;
        }
        return ApplicationType::fromRequest($GLOBALS['TYPO3_REQUEST']);
    }
}