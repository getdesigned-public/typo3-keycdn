<?php

declare(strict_types=1);
namespace Getdesigned\KeyCdn\Resource\EventListener;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use Getdesigned\KeyCdn\Resource\Driver\KeyCdnProxyDriver;
use Getdesigned\KeyCdn\Security\ImageProcessingToken;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Resource\AbstractFile;
use TYPO3\CMS\Core\Resource\Event\GeneratePublicUrlForResourceEvent;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use TYPO3\CMS\Core\Utility\HttpUtility;

/**
 * Adds the Secure Token for KeyCDN if applicable.
 *
 * @since 12.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
class PublicUrlTokenizer
{
    /**
     * Static property to avoid an infinite loop.
     *
     * @var bool
     */
    private static bool $isProcessingUrl = false;

    /**
     * Dependency injection.
     *
     * @param ExtensionConfiguration $extensionConfiguration Service for reading extension configurations
     */
    public function __construct(private readonly ExtensionConfiguration $extensionConfiguration) {}

    /**
     * Handles the event.
     *
     * @param GeneratePublicUrlForResourceEvent $event The event to be handled
     * @return void
     */
    public function __invoke(GeneratePublicUrlForResourceEvent $event): void
    {
        if (self::$isProcessingUrl) {
            return;
        }

        try {
            if (!$this->extensionConfiguration->get('key_cdn', 'enableCdnFiles')) {
                return;
            }
        } catch (ExtensionConfigurationExtensionNotConfiguredException|ExtensionConfigurationPathDoesNotExistException $e) {
            return;
        }

        $resource = $event->getResource();
        $storage = $event->getStorage();

        if ($resource instanceof ProcessedFile) {
            $resource = $resource->getOriginalFile();
        }

        if (
            !$resource instanceof File
            || AbstractFile::FILETYPE_IMAGE !== $resource->getType()
            || !$resource->isImage()
            || KeyCdnProxyDriver::SHORT_NAME !== $storage->getDriverType()
            || empty($storage->getConfiguration()['secureTokenKey'] ?? null)
            || !($storage->getConfiguration()['nativeFileProcessing'] ?? false)
        ) {
            return;
        }

        self::$isProcessingUrl = true;
        try {
            $urlParts = parse_url($resource->getPublicUrl());
            parse_str($urlParts['query'] ?? '', $queryString);
            if (isset($queryString['token'])) {
                return; // skip generation if token already is present
            }
            $queryString['token'] = ImageProcessingToken::fromResource($resource)->toBase64Hash();
            $urlParts['query'] = str_replace(['%2C'], [','], http_build_query($queryString, '', '&'));
            $event->setPublicUrl(HttpUtility::buildUrl($urlParts));
        } finally {
            self::$isProcessingUrl = false;
        }
    }
}