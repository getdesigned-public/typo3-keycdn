<?php

declare(strict_types=1);
namespace Getdesigned\KeyCdn\Resource\EventListener;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use Getdesigned\KeyCdn\Resource\Event\BeforeImageProcessingEvent;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\FileProcessingAspect;
use TYPO3\CMS\Core\Resource\Event\BeforeFileProcessingEvent;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Routes file processing of already processed files to KeyCDN if applicable.
 * This ensures files get properly processed through KeyCDN when delivery was disabled for previous requests.
 *
 * @since 12.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
class FileProcessingRouter
{
    /**
     * Service for reading extension configurations.
     *
     * @var ExtensionConfiguration
     */
    protected ExtensionConfiguration $extensionConfiguration;

    /**
     * Instantiates the event handler.
     */
    public function __construct()
    {
        $this->extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class);
    }

    /**
     * Handles the event.
     *
     * @param BeforeFileProcessingEvent $event The event passed
     * @return void
     */
    public function __invoke(BeforeFileProcessingEvent $event): void
    {
            // check if files should be delivered through KeyCDN
        try {
            if (!$this->extensionConfiguration->get('key_cdn', 'enableCdnFiles')) {
                return;
            }
        } catch (ExtensionConfigurationExtensionNotConfiguredException|ExtensionConfigurationPathDoesNotExistException $e) {
            return;
        }

        $processedFile = clone $event->getProcessedFile();
        $fileExtension = $processedFile->getOriginalFile()->getExtension();
        $excludedTypes = GeneralUtility::trimExplode(',', $this->extensionConfiguration->get('key_cdn', 'excludeProcessingForTypes'));

        if (in_array($fileExtension, $excludedTypes)) {
            return;
        }

        if (
                // if the file is not yet processed, we're done here
            !$processedFile->isProcessed()
                // if a processing URL already is set, this is probably because KeyCDN processing already is in use
            || !empty($processedFile->getProperty('processing_url'))
                // require a KeyCDN storage driver in use
            || !in_array($processedFile->getOriginalFile()->getStorage()->getDriverType(), ['KeyCdnProxy'], true)
                // require image processing through KeyCDN is enabled
            || !($processedFile->getOriginalFile()->getStorage()->getConfiguration()['nativeFileProcessing'] ?? false)
        ) {
            return;
        }

            // By setting the checksum to an empty value, the system recognizes the necessity to re-process the file,
            // but would delete the existing file in return. This would result in the file having to be re-processed
            // locally again whenever delivery via KeyCDN is disabled. To avoid this, we flag the file as already
            // deleted to keep the physical copy.
        $processedFile->updateProperties(['checksum' => '']);
        $processedFile->setDeleted();

        $event->setProcessedFile($processedFile);
    }

    /**
     * Pre-processes an image through KeyCDN if applicable.
     *
     * @param BeforeImageProcessingEvent $event Incoming event
     * @return void
     */
    public function preProcessImage(BeforeImageProcessingEvent $event): void
    {
        $processedFile = $event->getProcessedFile();

        if (
            !$this->extensionConfiguration->get('key_cdn', 'enableCdnFiles')
            || 'KeyCdnProxy' !== $processedFile->getStorage()->getDriverType()
            || !($processedFile->getStorage()->getConfiguration()['nativeFileProcessing'] ?? false)
        ) {
            return;
        }

        GeneralUtility::makeInstance(Context::class)->setAspect('fileProcessing', new FileProcessingAspect(false));

        $event->setProcessedFile($processedFile->getOriginalFile()->process(
            $processedFile->getTaskIdentifier(),
            $processedFile->getProcessingConfiguration()
        ));
    }
}