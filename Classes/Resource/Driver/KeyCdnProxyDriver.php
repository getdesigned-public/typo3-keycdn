<?php

declare(strict_types=1);
namespace Getdesigned\KeyCdn\Resource\Driver;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Resource\Driver\LocalDriver;
use TYPO3\CMS\Core\Resource\ResourceStorageInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;

/**
 * Driver utilizing KeyCDN to act as a kind of proxy by serving resources from the local file system.
 *
 * @since 12.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
class KeyCdnProxyDriver extends LocalDriver
{
    /**
     * The short name of the driver.
     */
    public const SHORT_NAME = 'KeyCdnProxy';

    /**
     * Service for reading extension configurations.
     *
     * @var ExtensionConfiguration
     */
    protected ExtensionConfiguration $extensionConfiguration;

    /**
     * @inheritdoc
     */
    public function __construct(array $configuration = [])
    {
        parent::__construct($configuration);

        $this->extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class);
    }

    /**
     * @inheritdoc
     */
    protected function determineBaseUrl(): void
    {
            // only calculate baseURI if the storage does not enforce jumpUrl Script
        if (!$this->hasCapability(ResourceStorageInterface::CAPABILITY_PUBLIC)) {
            return;
        }

            // only use configured baseURI if file delivery through KeyCDN is enabled
        try {
            if (
                $this->extensionConfiguration->get('key_cdn', 'enableCdnFiles')
                && !empty($this->configuration['baseUri'])
            ) {
                $this->baseUri = rtrim($this->configuration['baseUri'], '/') . '/';
                return;
            }
        } catch (ExtensionConfigurationExtensionNotConfiguredException|ExtensionConfigurationPathDoesNotExistException $e) {}

            // use site-relative URLs
        if (str_starts_with($this->absoluteBasePath, Environment::getPublicPath())) {
            $temporaryBaseUri = rtrim(PathUtility::stripPathSitePrefix($this->absoluteBasePath), '/');
            if ('' !== $temporaryBaseUri) {
                $uriParts = explode('/', $temporaryBaseUri);
                $uriParts = array_map('rawurlencode', $uriParts);
                $temporaryBaseUri = implode('/', $uriParts) . '/';
            }
            $this->baseUri = $temporaryBaseUri;
        }
    }
}