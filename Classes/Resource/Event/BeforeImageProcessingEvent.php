<?php

declare(strict_types=1);
namespace Getdesigned\KeyCdn\Resource\Event;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use TYPO3\CMS\Core\Resource\ProcessedFile;

/**
 * Event being called before the \TYPO3\CMS\Core\Resource\Service\ImageProcessingService processes an image.
 *
 * @since 12.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
final class BeforeImageProcessingEvent
{
    /**
     * Instantiates the event.
     *
     * @param ProcessedFile $processedFile The file to be processed
     */
    public function __construct(private ProcessedFile $processedFile) {}

    /**
     * Returns the file to be processed.
     *
     * @return ProcessedFile The file to be processed
     */
    public function getProcessedFile(): ProcessedFile
    {
        return $this->processedFile;
    }

    /**
     * Sets the file to be processed.
     *
     * @param ProcessedFile $processedFile The file to set
     * @return void
     */
    public function setProcessedFile(ProcessedFile $processedFile): void
    {
        $this->processedFile = $processedFile;
    }
}