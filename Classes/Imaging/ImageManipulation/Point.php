<?php

declare(strict_types=1);
namespace Getdesigned\KeyCdn\Imaging\ImageManipulation;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\Processing\TaskInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Specifies a point of an image.
 *
 * @since 12.0.0
 * @author Daniel Haring <dh@getdesigned.at>
 */
class Point
{
    /**
     * The x-coordinate of the point.
     *
     * @var float
     */
    protected float $x;

    /**
     * The y-coordinate of the point.
     *
     * @var float
     */
    protected float $y;

    /**
     * Instantiates a new point.
     *
     * @param float $x The x-coordinate of the point
     * @param float $y The y-coordinate of the point
     */
    public function __construct(float $x, float $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * Creates a focal point for a processing task.
     *
     * @param TaskInterface $task The task to create the focal point for
     * @return self|null The focal point created, if any
     */
    public static function createFromProcessingTask(TaskInterface $task): ?self
    {
        $configuration = $task->getTargetFile()->getProcessingConfiguration();

        $x = $configuration['focus_point_x'] ?? null;
        $y = $configuration['focus_point_y'] ?? null;

        if (null === $x || null === $y) {
            return null;
        }

        return new self((float)$x, (float)$y);
    }

    /**
     * Creates a focal point for a file.
     *
     * @param FileInterface $file The file to create the focal point for
     * @return self|null The focal point created, if any
     */
    public static function createFromFile(FileInterface $file): ?self
    {
        $x = $file->hasProperty('focus_point_x') ? $file->getProperty('focus_point_x') : null;
        $y = $file->hasProperty('focus_point_y') ? $file->getProperty('focus_point_y') : null;

        if (null !== $x && null !== $y) {
            return new self((float)$x, (float)$y);
        }

        if (!method_exists($file, 'getOriginalFile')) {
            return null;
        }

        $file = $file->getOriginalFile();
        if (!$file instanceof FileInterface) {
            return null;
        }

        $x = $file->hasProperty('focus_point_x') ? $file->getProperty('focus_point_x') : null;
        $y = $file->hasProperty('focus_point_y') ? $file->getProperty('focus_point_y') : null;

        return null !== $x && null !== $y ? new self((float)$x, (float)$y) : null;
    }

    /**
     * Creates a point with empty coordinates.
     *
     * @return self The point created
     */
    public static function createEmpty(): self
    {
        return new self(0.0, 0.0);
    }

    /**
     * Returns the left offset (x-coordinate) of the point.
     *
     * @return float The left offset (x-coordinate) of the point
     */
    public function getOffsetLeft(): float
    {
        return $this->x;
    }

    /**
     * Returns the top offset (y-coordinate) of the point.
     *
     * @return float The top offset (y-coordinate) of the point
     */
    public function getOffsetTop(): float
    {
        return $this->y;
    }

    /**
     * Checks whether the point does not point to actual coordinates.
     *
     * @return bool TRUE if the point does not point to actual coordinates, otherwise FALSE
     */
    public function isEmpty(): bool
    {
        return 0.0 === $this->x && 0.0 === $this->y;
    }

    /**
     * Returns the array representation of the point.
     *
     * @return array The array representation of the point
     */
    public function asArray(): array
    {
        return [
            'x' => $this->x,
            'y' => $this->y
        ];
    }

    /**
     * Returns the KeyCDN focal point crop instructions according to the current coordinates.
     *
     * @return string The KeyCDN focal point crop instructions
     */
    public function toCropFocalPoint(): string
    {
        try {
            $cmd = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('key_cdn', 'debugFocalPoint') ? 'fpd' : 'fp';
        } catch (ExtensionConfigurationExtensionNotConfiguredException|ExtensionConfigurationPathDoesNotExistException $e) {
            $cmd = 'fp';
        }

        return sprintf(
            '%s,%s,%s',
            $cmd,
            round(($this->x + 100) * 0.005, 2),
            round(($this->y - 100) * -0.005, 2)
        );
    }

    /**
     * Returns the string representation of the point.
     *
     * @return string The string representation of the point
     */
    public function __toString(): string
    {
        if ($this->isEmpty()) {
            return '';
        }
        return json_encode($this->asArray());
    }
}